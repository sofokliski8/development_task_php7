Project installation
--------------------
1) Make sure you have installed [Git](https://git-scm.com/downloads), [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html)

2) Copy the project from your bitbucket account and clone it: 

`git clone https://<YOUR-ACCOUNT>/development_task_php7.git`

3) Move to your project directory
```
cd development_task_php7
```

4) Start the web-server with vagrant
```
vagrant up
```

5) SSH to your vagrant web-server
```
vagrant ssh
```

6) Move to your project directory inside vagrant
```
cd /vagrant
```

7) Install dependencies
```
composer install
```

8) Then you can have a view of printed data in your [localhost:port](localhost:port) page from your browser for example: [localhost:4000](localhost:4000)  
or in your command line by typing :
```
php index.php
```

Running Tests
-------------
Make sure you are inside the VM (vagrant) `vagrant ssh` & `cd /vagrant`
```
php ./vendor/bin/phpunit
```