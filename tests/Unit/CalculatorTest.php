<?php

declare(strict_types=1);

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

require "vendor/autoload.php";

use MyApp\Calculator;

class CalculatorTest extends TestCase
{
    const FOO_VALUE = 3;
    const BAR_VALUE = 5;
    const FOO_MESSAGE = ':Foo';
    const BAR_MESSAGE = ':Bar';
    const MIN_VALUE = 1;
    const MAX_VALUE = 100;

    /**
     * Initializes an object of class Calculator
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->calculatorTest = new Calculator();
    }

    /**
     * Tests if right exception is returned when invalid values (out of range 1-100) are given   
     * @test
     * @dataProvider invalid_numbers_cases_OutOfRange
     */
    public function checks_returned_table_with_invalid_numbers_out_of_range_startCalculator(
        int $firstNum,
        int $secondNum
    ): void {
        $this->expectExceptionMessage('Numbers must be in range of (' . self::MIN_VALUE . '-' . self::MAX_VALUE . ')');
        $this->calculatorTest->startCalculator($firstNum, $secondNum);
    }

    /**
     * Tests if right exception is returned when invalid values (number2 > number1) are given  
     * @test
     * @dataProvider invalidNumbersCases_isBigger
     */
    public function checks_returned_table_with_invalid_numbers_isBigger_startCalculator(
        int $firstNum,
        int $secondNum
    ): void {
        $this->expectExceptionMessage('Second number must be bigger than first number.');
        $this->calculatorTest->startCalculator($firstNum, $secondNum);
    }

    /**
     * Tests if the right number of numbers between number1 and number2 are created.
     * @test
     * @dataProvider validNumbersCases
     */
    public function number_of_numbers_startCalculator(
        int $firstNum,
        int $secondNum
    ): void {
        $count = ($secondNum - $firstNum) + 1;
        $this->assertCount($count, $this->calculatorTest->startCalculator($firstNum, $secondNum));
    }

    /**
     * Contains valid numbers (in range MIN_VALUE - MAX_VALUE).
     */
    public function validNumbersCases(): array
    {
        return [
            [self::MIN_VALUE, self::MAX_VALUE],
            [self::MIN_VALUE, self::MIN_VALUE],
            [self::MAX_VALUE, self::MAX_VALUE],
            [intdiv(self::MAX_VALUE, 2), intdiv(self::MAX_VALUE, 2)]
        ];
    }

    /**
     * Contains invalid numbers (out of range MIN_VALUE - MAX_VALUE).
     */
    public function invalid_numbers_cases_OutOfRange(): array
    {
        return [
            [self::MIN_VALUE - 1, self::MAX_VALUE],
            [self::MIN_VALUE, self::MAX_VALUE + 1],
            [self::MIN_VALUE, self::MAX_VALUE * 100],
            [self::MIN_VALUE - 1000000, self::MAX_VALUE]
        ];
    }

    /**
     * Contains invalid numbers (number1 > number2).
     */
    public function invalidNumbersCases_isBigger(): array
    {
        return [
            [self::MAX_VALUE, self::MIN_VALUE],
            [self::MIN_VALUE + 1, self::MIN_VALUE],
            [intdiv(self::MAX_VALUE, 2), intdiv(self::MIN_VALUE, 2) + 1]
        ];
    }
}