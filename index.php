<?php

declare(strict_types=1);

require "vendor/autoload.php";

use MyApp\Calculator;

$testvar = new Calculator(); //Object of type Calculator
?>

<!DOCTYPE html>
<html>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<section class="container ">
    <h4>Calculator</h4>

    <span>Give two integers in range of (1-100)</span>
    <form action="index.php" method="post">
        <div class="input-group">
            <input type="number" placeholder="First number" class="form-control" name="number1">
            <input type="number" placeholder="Second number" class="form-control" name="number2">
            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
        </div>
    </form>

    <?php
    if (isset($_POST['submit'])) {
        try {
            /**
             * Converts input value from string to integer and calls startCalculator function and then prints the numbers
             */
            $number1 = intval($_POST['number1']);
            $number2 = intval($_POST['number2']);
            $tableDemo = $testvar->startCalculator($number1, $number2);

            foreach ($tableDemo as $item) {
                echo '<ul class="list-group">
                            <li class="list-group-item">' . $item . '</li>
                        </ul>';
            }
        } catch (\InvalidArgumentException $ex) {
            $errormessage = $ex->getMessage();
            echo '<div class="alert alert-danger" role="alert">' . $errormessage . '</div>';
        }
    }
    ?>
</section>