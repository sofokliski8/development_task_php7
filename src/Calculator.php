<?php

declare(strict_types=1);

namespace MyApp;

class Calculator
{
    const MIN_VALUE = 1;
    const MAX_VALUE = 100;
    const FOO_MESSAGE = ' :Foo';
    const BAR_MESSAGE = ' :Bar';
    const FOO_VALUE = 3;
    const BAR_VALUE = 5;

    /**
     * If given numbers are valid (in range 1-100) and second number is not greater than first number
     *  we call createData function and we take an array with numbers in return 
     * @return array | null 
     */
    public function startCalculator(int $number1, int $number2): ?array
    {
        if ($this->areValid($number1, $number2)) {
            return $this->createData($number1, $number2);
        }
        return null;
    }

    /**
     * Checks if given numbers are valid (In range of (0-100) and number1 > number2).
     *  if true then return true
     *  else throws InvalidArgumentException
     * @return bool
     * @throws \InvalidArgumentException
     */
    private function areValid(int $number1, int $number2): bool
    {
        if (!$this->isInRange($number1) || !$this->isInRange($number2)) {
            throw new \InvalidArgumentException('Numbers must be in range of (' . self::MIN_VALUE . '-' . self::MAX_VALUE . ')');
        } elseif ($this->isBigger($number1, $number2)) {
            throw new \InvalidArgumentException('Second number must be bigger than first number.');
        }
        return true;
    }

    /**
     * Checks if number1 > number2
     *  if true then return true
     *  else false
     * @return bool
     */
    private function isBigger(int $number1, int $number2): bool
    {
        return $number1 > $number2;
    }

    /**
     * Checks if given number is in range of (MIN_VALUE - MAX_VALUE)
     * @return bool
     */
    private function isInRange(int $number): bool
    {
        return ($number >= self::MIN_VALUE && $number <= self::MAX_VALUE);
    }

    /**
     * Starting from number1 to number2, appends to an array the number with a corresponding word from wordPrinter
     * @return array 
     */
    private function createData(int $number1, int $number2): array
    {
        $array = [];
        while ($number1 <= $number2) {
            $array[] = $number1 . $this->wordPrinter($number1);
            $number1++;
        }
        return $array;
    }

    /**
     * Checks if given number is multiple of FOO_VALUE or BAR_VALUE, and returns the corresponding word 
     * @return string 
     */
    private function wordPrinter(int $number): string
    {
        $properMessage = '';
        if ($this->modChecker($number, self::FOO_VALUE)) {
            $properMessage = self::FOO_MESSAGE;
        }
        if ($this->modChecker($number, self::BAR_VALUE)) {
            $properMessage = $properMessage . self::BAR_MESSAGE;
        }
        return $properMessage;
    }

    /**
     * Checks if given number is multiple of given value
     * @return bool
     */
    private function modChecker(int $number, int $value)
    {
        return $number % $value === 0;
    }
}